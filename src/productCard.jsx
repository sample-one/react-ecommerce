
function ProductCard(props){
  console.log(props.title)
  return(
<article className="product">
            <img
              src={props.image?props.image:"https://icrier.org/wp-content/uploads/2022/09/Event-Image-Not-Found.jpg"}
            />
            <div className="productDetails">
              <h3 className="h6">{props.title}</h3>
              <div>
                <span>★</span>
                <span>★</span>
                <span>★</span>
                <span>☆ </span>
                <span>☆ </span>
              </div>
              <div className="priceAndButton">
                <span className="p">${props.price}</span>
                <button className="button buttonPrimary">Add to cart</button>
              </div>
            </div>
          </article>
  )
}
export default ProductCard