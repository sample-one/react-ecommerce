import Header from './Header'
import './App.css'
import ProductCard from './productCard'

function App() {
  
  return (
    <>
      <Header/>
  
  <main>
    <section id="section1">
      <div className="container">
        <div id="images">
          <img
            id="image1"
            src="https://img.freepik.com/free-photo/young-stylish-beautiful-woman-blue-printed-dress-sunglasses-happy-mood-fashion-outfit-trendy-apparel-smiling-summer-accessories_285396-3845.jpg?t=st=1716440616~exp=1716444216~hmac=25d7e63b06c438a3d92a98ae3697281fb24ce9cd79cef5c623ce77d93d086411&w=360"
            alt=""
          />
          <img
            id="image2"
            src="https://img.freepik.com/free-photo/beautiful-blonde-woman-with-floral-dress_144627-24706.jpg?t=st=1716440877~exp=1716444477~hmac=33050edfbcc079dcc1548678a05edc8e691b1dc7e30204016f1701c782e823d3&w=360"
            alt=""
          />
          <img
            id="image3"
            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAABlBMVEXY2Njo6Oie6BDVAAABDUlEQVR4nO3PAQ0AIAzAsOPfNCoIGbQKtpnHrdsBxznsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77Pjjcs3wA4s0RAgUAAAAASUVORK5CYII="
            alt=""
          />
          <img
            id="image4"
            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAABlBMVEXY2Njo6Oie6BDVAAABDUlEQVR4nO3PAQ0AIAzAsOPfNCoIGbQKtpnHrdsBxznsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77Pjjcs3wA4s0RAgUAAAAASUVORK5CYII="
            alt=""
          />
          <img
            id="mainImage"
            src="https://img.freepik.com/free-photo/smiling-blonde-young-woman-floral-dress-posing-florist-shop_23-2148049366.jpg?t=st=1716440297~exp=1716443897~hmac=753ce80c15ce44053d59ff809230972254bfda11ebac6419a5476c34bb2f7c01&w=360"
            alt=""
          />
        </div>
        <div>
          <h1 id="productTitle">Short Smocking Printed Cami Dress</h1>
          <span className="h3 price">
            MRP ₹<span id="price">1,490</span>
          </span>
          <ul className="variants">
            <li className="variant">
              <img
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAABlBMVEXY2Njo6Oie6BDVAAABDUlEQVR4nO3PAQ0AIAzAsOPfNCoIGbQKtpnHrdsBxznsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77Pjjcs3wA4s0RAgUAAAAASUVORK5CYII="
                alt=""
              />
            </li>
            <li className="variant">
              <img
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAABlBMVEXY2Njo6Oie6BDVAAABDUlEQVR4nO3PAQ0AIAzAsOPfNCoIGbQKtpnHrdsBxznsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77Pjjcs3wA4s0RAgUAAAAASUVORK5CYII="
                alt=""
              />
            </li>
            <li className="variant">
              <img
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAABlBMVEXY2Njo6Oie6BDVAAABDUlEQVR4nO3PAQ0AIAzAsOPfNCoIGbQKtpnHrdsBxznsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77Pjjcs3wA4s0RAgUAAAAASUVORK5CYII="
                alt=""
              />
            </li>
            <li className="variant">
              <img
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAABlBMVEXY2Njo6Oie6BDVAAABDUlEQVR4nO3PAQ0AIAzAsOPfNCoIGbQKtpnHrdsBxznsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77Pjjcs3wA4s0RAgUAAAAASUVORK5CYII="
                alt=""
              />
            </li>
          </ul>
          <p id="productDescription">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ratione
            doloremque explicabo cum. Iste aspernatur alias quidem ullam, velit
            optio vero itaque. Aliquid deserunt eum incidunt nemo qui quo, rerum
            ad? Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Voluptatem necessitatibus hic dicta suscipit consequatur velit
            perferendis eius pariatur fuga fugiat. Nostrum hic corporis magnam
            velit beatae iusto officiis asperiores totam?
          </p>
          <div className="buttonHolder">
            <button className="button buttonPrimary">Add to cart</button>
            <a className="button buttonSecondary" href="#">
              Buy now
            </a>
          </div>
        </div>
      </div>
    </section>
    <section>
      <div className="container">
        <h2>Similar products</h2>
        <div className="productsList" id="listingDiv">
          <ProductCard title="Green Waist Cut-Out Cotton Dress" price={234} image="https://plus.unsplash.com/premium_photo-1683134633584-55abb712f9a6?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MXx8R3JlZW4lMjBXYWlzdCUyMEN1dCUyME91dCUyMENvdHRvbiUyMERyZXNzJTIwd29tZW4lMjBkcmVzc3xlbnwwfHwwfHx8MA%3D%3D"/>
          <ProductCard  title="Comfy Cotton Sleeveless Overshirt" price={567} image="https://img.freepik.com/free-photo/woman-wearing-white-tank-top_53876-102227.jpg?t=st=1716439383~exp=1716442983~hmac=6e58a623bb76da2e02c2435c8c713cea8d97db089924881df8380348b6914b1c&w=360"/>
          <ProductCard title="Airy Cotton Fit And Flare Dress" price={345} image="https://img.freepik.com/free-photo/young-beautiful-woman-home_144627-28477.jpg?t=st=1716439610~exp=1716443210~hmac=d62c262f73ac9a16240258308c9fb6535eb2c3882467df2437472336a268d43c&w=360"/>
          <ProductCard title="Pretty Cotton  Tiered Dress" price={204} image="https://img.freepik.com/free-photo/beautiful-woman-portrait-garden_1328-1852.jpg?t=st=1716440144~exp=1716443744~hmac=0f1bfd53b50ed204393b45756d2137aeee398446b8e5522512ace3987debfebc&w=360" />
          
          <ProductCard title="Pretty Cotton Schiffli Tiered Dress" price={234} image="" />
        </div>
      </div>
    </section>
  </main>
</>

    
  )
}

export default App
